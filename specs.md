# Rough bot draft

- Written in Typescript, hosted in a free Oracle VPS (lmao thank you idiots). Using filesystem for database content for now.
- VPS will be maintained by bot developer(s). Direct access is only granted to those who can be trusted not to break the whole thing :P
- Using Slash commands for bot interactions. No support for text commands.
- Designing the bot with the possibility of using in multiple servers in mind.
- Many commands can be only used by admins. They will use the 1141521674815164506 (mod) role for the check.

## Poll workflow

A poll is initialized when a command requires consensus from the community. The content of the poll depends on the specifics, but generally will be:
- A ping to the voter role (outside the embed since Discord is broken). The role ID is 1131921500312699014.
- The actual content of the poll, telling the user what exactly is being decided on.
- The time when the poll is finished is displayed (in special time format, so everyone gets it in their local time).
- A listbox of the available options. When one is selected, the poll vote is cast.
	- The user will get a confirmation as an ephemeral message.
	- The vote will automatically be anonymous
	- The subject of the poll will be ignored.
	- The vote can be changed until the poll is complete.
- Majority agreement is required.
	- In case of a tie, the poll will be restarted
	- All the options that have not tied with the majority tie will be removed.
- The action will be done after the poll has concluded.
- In case the bot crashes, it will keep a log of who has voted. That said, some votes may be lost... Need to think of a protocol on how to handle this properly.
- If a poll has concluded while the bot has been down, the poll will be concluded in the first possible moment after starting up. Its effects will be handled right after the poll has concluded.
- When the poll concludes, the results are shown and no more votes can be cast.
- The poll will allow users to change their voting or cancel it completely.

## `/timeout <time> <user>`

- Admin only.
- Times out an user for <time>.
- If <time> is more than 1 minute, a poll is required for majority to agree to the poll.
- The maximum timeout is 1 day.
- The length of the poll is is 25% of the length of the timeout, to give people enough time to notice the poll.
- The timeout is applied from the moment the poll finishes.
- The user must not be currently muted.
- Options: `time out`, `do not time out`

## `/rehab <user>`

- Admin only.
- Only 1 use per user per day.
- A 24-hour poll to decide whether to rehab <user>.
- Sends <user> to rehab and removes their mod roles.
- Options: `abstain`, `keep in rehab`, `let out of rehab`
- When the user is let out of rehab, their mod role is reinstated.

## `/unpunish <user>`

- Admin only.
- Only 1 use per user per day.
- A 24-hour poll to decide whether to unpunish <user>.
- Options: `abstain`, `yes`, `no`
- If yes wins, <user> is unbanned, unmuted, and let out of rehab.

## `/punish <user>`

- Admin only.
- Only 1 use per user per day.
- A 24-hour poll to decide the action taken.
- Sends <user> to rehab and removes their mod roles.
- Options:
	- `abstain` - Vote does not count.
	- `ban` - If this wins, <user> will be banned.
	- `kick` - If this wins, <user> will be kicked.
	- `mute` - If this wins, <user> will be permanently muted. Will not do anything if the user is no longer in the server.
	- `rehab` - If this wins, <user> will be kept in rehab. Will not do anything if the user is no longer in the server.
	- `no punishment` - If this wins, <user> will be let out of rehab and mod role will be reinstated.

## `/role give <user>` and `/role take <user>`

- Admin only.
- Only 1 use per user per day.
- Either gives or takes a role from <user> who must be currently on the server.
- Only special roles can be given. These roles are: 1141521674815164506 (mod)
- <user> can not give themselves roles, only take their roles away.
