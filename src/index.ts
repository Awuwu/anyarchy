import { ActivityType, Client, GatewayIntentBits, Partials, PresenceUpdateStatus, Snowflake } from "discord.js";
import fs from "fs";
import fsp from "fs/promises";
import { initCommandHandler, sendCommands } from "./commands/slash";
import { initPolls } from "./tools/polls";
import { initTimeouts } from "./tools/timeout";
import { loadDatabases, saveDatabases } from "./tools/db";
import { _TOMS } from "./tools/time";
import { endTimeouts, intervals } from "./tools/timers";

// eslint-disable-next-line multiline-comment-style
// CAN CRASH IF NOT HERE
// @ts-expect-error
global.bot = undefined;

/**
 * Lazy error handling module. ALL OF THIS IS TEMPORARY!!!
 */
export const e = (e:unknown): void => {
	const after = () => {
		console.error(e);
		process.exit(-1);
	}

	// If the bot was initialized, then force it to quit.
	if(bot) {
		bot.destroy().then(after).catch(after);

	} else {
		after();
	}
}

/**
 * Poll length data. In seconds.
 */
export type PollLengthContents = {
	unpunish: number;
	punish: number;
	rehab: number;
	timeout: number;		// Actually a multiplier.
};

/**
 * User command rate-limits in seconds. Stores the Unix timestamps for different functions when the command is available again.
 */
export type RatelimitContents = {
	unpunish: number;
	punish: number;
	timeout: number;
	role: number;
};

export type ServerRoleData = {
	// List of "special" roles in this server. These can be given via commands.
	specialRoles: Snowflake[];
	// List of admin roles. This is used to limit command usage.
	adminRoles: Snowflake[];
	// List of roles to ping for any poll.
	pollRoles: Snowflake[];
	// List of roles that are not allowed to vote.
	cantVote: Snowflake[];
};

/**
 * Type of the bot configuration file.
 */
export type Config = {
	// Bot log-in information.
	login: {
		token: string;
		id: string;
	};
	autosaveSeconds: number;
	servers: { [key: Snowflake]: ServerRoleData & {
		// The channel to sends polls to.
		pollChannel: Snowflake;
		// Number of seconds for rate-limiting each different type of command.
		ratelimit: RatelimitContents;
		// Number of seconds for different poll lengths.
		pollLengths: PollLengthContents;
	}};
}

async function _exit() {
	endTimeouts();

	// If the bot was initialized, then force it to quit.
	if(bot) {
		await bot.destroy();
	}

	// save databases
	await saveDatabases();

	// quit
	process.exit(0);
}

process.on("exit", () => _exit().catch(e));
process.on("unhandledRejection", e);
process.on("uncaughtException", e);

// Initialize the Discord client
global.bot = new Client({
	intents: GatewayIntentBits.GuildModeration | GatewayIntentBits.GuildMessages | GatewayIntentBits.GuildMembers,
	partials: [ Partials.Message, ],
	presence: {
		status: PresenceUpdateStatus.Online,
		activities: [
			{
				name: "the world burn",
				type: ActivityType.Watching,
			},
		],
	},
});

// Load the config file into memory
fs.readFile("bot.json", undefined, (err, data) => {
	if(err) {
		bot.destroy().then(() => e(err)).catch(e);
		return;
	}

	// Convert the config file into a javascript object
	global.config = JSON.parse(data.toString()) as Config;

	// Log into the bot and start the related systems.
	bot.login(config.login.token).then(() => {
		bot.on("ready", async() => {
			// Create database folder.
			if(!fs.existsSync("db")) {
				await fsp.mkdir("db", { recursive: true, });
			}

			await loadDatabases();
			initPolls();

			// Load commands.
			await sendCommands();
			initCommandHandler();

			// set up an interval for autosave of database
			intervals.push(setInterval(() => saveDatabases().catch(e), config.autosaveSeconds * _TOMS));

			// Initialize timeout handling for every user
			await initTimeouts().catch(e);
		});
	}).catch(e);
})
