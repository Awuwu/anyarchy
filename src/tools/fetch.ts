import { APIInteractionGuildMember, Awaitable, Guild, GuildBasedChannel, GuildMember, Message, Snowflake, TextBasedChannel, User } from "discord.js";

/**
 * Convert a partial member to a full member
 *
 * @param guild The guild where this member should be
 * @param member The member to convert
 * @returns null or a full member
 */
export function toFullMember(guild: Guild|null, member: GuildMember|APIInteractionGuildMember|null): Awaitable<GuildMember|null> {
	if(!member) {
		return null;
	}

	// If this is already a correct member, return it
	if("guild" in member && (!guild || member.guild.id === guild?.id)) {
		return member;
	}

	if(!guild) {
		return null;
	}

	// fetch the member
	return fetchMember(guild, member.user.id);
}

/**
 * Convert an user to a full member.
 *
 * @param guild The guild where this user should be
 * @param user The user to convert
 * @returns null or a full member
 */
// eslint-disable-next-line require-await
export async function toMember(guild: Guild|null, user: User|null): Promise<GuildMember|null> {
	if(!user || !guild) {
		return null;
	}

	// fetch the member
	return fetchMember(guild, user.id);
}

/**
 * Parse a message URL to get its important parts
 *
 * @param url The URL to use
 */
export function parseMessageURL(url: string): { message: Snowflake, channel: Snowflake, guild: Snowflake, } {
	// Grab the individual URL components.
	const msg = new URL(url).pathname.split("/");
	return { message: msg[4], channel: msg[3], guild: msg[2], };
}

/**
 * Fetch a guild based on message url.
 *
 * @param url The URL to use
 */
export function fetchGuildFromURL(url: string): Promise<Guild> {
	// Grab the individual URL components.
	return bot.guilds.fetch(parseMessageURL(url).guild);
}

/**
 * Fetch a channel based on message url.
 *
 * @param url The URL to use
 */
export async function fetchChannelFromURL(url: string): Promise<null|GuildBasedChannel> {
	// Grab the individual URL components.
	const d = parseMessageURL(url);
	const guild = await bot.guilds.fetch(d.guild);
	return fetchChannel(guild, d.channel);
}

/**
 * Fetch a text-based channel based on message url.
 *
 * @param url The URL to use
 */
export async function fetchTextChannelFromURL(url: string): Promise<null|TextBasedChannel> {
	// Fetch the channel first.
	const channel = await fetchChannelFromURL(url);

	if(!channel || !channel.isTextBased()) {
		return null;
	}

	return channel;
}

/**
 * Fetch a message based on its url.
 *
 * @param url The URL to use
 */
export async function fetchMessage(url: string): Promise<null|Message> {
	// Fetch the channel first.
	const channel = await fetchTextChannelFromURL(url);

	if(!channel) {
		return null;
	}

	try {
		// Grab the actual message
		return channel.messages.fetch(parseMessageURL(url).message);
	} catch(_) {
		return null;
	}
}

/**
 * Safely fetch a channel from a guild.
 */
export async function fetchChannel(guild: Guild, id: Snowflake): Promise<GuildBasedChannel|null> {
	try {
		return await guild.channels.fetch(id);
	} catch(_) {
		return null;
	}
}

/**
 * Safely fetch a text channel from a guild.
 */
export async function fetchTextChannel(guild: Guild, id: Snowflake): Promise<TextBasedChannel|null> {
	const channel = await fetchChannel(guild, id);

	if(!channel || !channel.isTextBased()) {
		return null;
	}

	return channel;
}

/**
 * Safely fetch member from a guild.
 */
export async function fetchMember(guild: Guild, id: Snowflake): Promise<GuildMember|null> {
	try {
		return await guild.members.fetch(id);
	} catch(_) {
		return null;
	}
}
