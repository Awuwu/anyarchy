import { Snowflake } from "discord.js";
import { RatelimitContents, e } from "..";
import fs from "fs";
import fsp from "fs/promises";

export type PollType = "rehab" | "punish" | "unpunish" | "timeout";

export type DatabasePoll = {
	// An ID used to detect poll info. Usually interaction base ID.
	id: string;
	// Unix timestamp for when the poll ends.
	ends: number;
	// The type of the poll. Poll contains extra data and information relating to the poll type.
	type: PollType;
	// Target of the poll. This is typically a user ID.
	target: Snowflake;
	// URL of the actual message.
	url: string;
	/*
	 * Tracks who has voted for what poll.
	 * Avoids issues where an user votes multiple times due to bot error or changes to abstain.
	 * Abstain votes are not listed.
	 */
	votes: { [key: string]: Snowflake[], };
};

export type DatabaseRehabPunishPoll = DatabasePoll & {
	// The roles that were removed before the poll
	removedRoles: Snowflake[];
};

export type DatabaseTimeoutPoll = DatabasePoll & {
	// The length of the timeout in milliseconds
	length: number;
};

/**
 * Bot database user contents.
 */
export type DatabaseUsers = {
	// The rate-limits for the user.
	ratelimits: RatelimitContents;
};

export type DatabaseTimeout = {
	// Whether there is a timeout for the user. This needs to be fetched from another table.
	timeout: boolean;
	// Roles removed when the user was timed out
	timeoutRemovedRoles: Snowflake[];
};

/**
 * Bot database file contents.
 */
export type Database = {
	// List of currently active polls.
	polls: DatabasePoll[];
	// The user-related data
	users: { [key: Snowflake]: DatabaseUsers & DatabaseTimeout, };
};

/**
 * Function to load databases for all the guilds.
 */
export async function loadDatabases(): Promise<void> {
	global.db = {};

	// Fetch and map for all guilds the bot is in.
	await Promise.all((await bot.guilds.fetch()).map(async(_, id) => {
		const tryLoad = async(ext: string) => {
			try {
				const file = "db/"+ id +".json" + ext;

				if(fs.existsSync(file)) {
					// If the database file exists, parse it.
					db[id] = JSON.parse((await fsp.readFile(file)).toString());
					return true;
				}

				return false;

			} catch(ex) {
				return ex;
			}
		}

		// Try to load the default file.
		const error1 = await tryLoad("");
		if(error1 === true) {
			return;
		}

		// Try to load the temporary file.
		const error2 = await tryLoad(".temp");
		if(error2 === true) {
			return;
		}

		// Check if either of these crashed. if so, we should instead bail out asap.
		if(!(error1 === false && error2 === false)) {
			e(error1 === false ? error2 : error1);
		}

		// The files simply dont exist. We should create default config.
		db[id] = {
			polls: [],
			users: {},
		};
	}));
}

/**
 * Function to save databases for all the guilds.
 */
export async function saveDatabases(): Promise<void> {
	// Fetch and map for all guilds the bot is in.
	await Promise.all((await bot.guilds.fetch()).map(async(_, id) => {
		const file = "db/"+ id +".json";

		if(fs.existsSync(file)) {
			// Copy current database into .old file for safekeeping.
			await fsp.copyFile(file, file +".old");
		}

		// Create the new json data into a .temp file
		await fsp.writeFile(file + ".temp", JSON.stringify(db[id]));

		// Then replace the actual file with the temp file. This *should* be an atomic rename.
		await fsp.rename(file +".temp", file);
	}));
}

/**
 * Initialize db for an user.
 *
 * @returns The database requested
 */
export function getDB(guild: Snowflake, member: Snowflake): DatabaseUsers & DatabaseTimeout {
	if(!db[guild]) {
		throw new Error("Have not initialized guild "+ guild +" database");
	}

	if(!db[guild].users[member]) {
		// Initialize
		db[guild].users[member] = {
			timeoutRemovedRoles: [],
			timeout: false,
			ratelimits: {
				unpunish: 0,
				punish: 0,
				timeout: 0,
				role: 0,
			},
		};
	}

	return db[guild].users[member];
}
