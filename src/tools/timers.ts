import { Awaitable, GuildMember } from "discord.js";


export const intervals: NodeJS.Timeout[] = [];
export const timeouts: { [key:string]: NodeJS.Timeout, } = {};

/**
 * End all the currently active timeouts.
 */
export function endTimeouts(): void {
	intervals.forEach((i) => clearInterval(i));
	Object.values(timeouts).forEach((i) => clearTimeout(i));
}

export type TimeoutFunctions = {
	/**
	 * Returns a boolean representing if the timeout is active.
	 */
	active: () => boolean,
	/**
	 * End the timeout.
	 *
	 * @returns Whether the timeout was running
	 */
	cancel: () => boolean,
	/**
	 * Start or restart the timeout.
	 *
	 * @param length The number of milliseconds to run the timeout for
	 * @param cb The callback to run after the timeout is done.
	 * @returns Whether the timeout was running already
	 */
	start: (length: number, cb: () => Awaitable<unknown>) => boolean,
}

/**
 * Create a simple handler to help with providing appropriate feedback to the
 * caller about the status a timeouts.
 *
 * @param type Special string that helps differentiate different timeouts for a single member.
 * @param target The target member to use the timeout for.
 * @param error The error handler that will receive any errors that may be otherwise missed.
 * @returns A set of helper functions.
 */
export function idTimeout(id: string, error?: (ex: unknown) => Awaitable<unknown>): TimeoutFunctions {
	const fullid = "ID"+ id;

	const active = () => id in timeouts;

	const cancel = () => {
		if(!active()) {
			return false;
		}

		// Timeout must be cancelled
		clearTimeout(timeouts[fullid]);
		delete timeouts[fullid];
		return true;
	}

	return {
		active,
		cancel,
		start: (length: number, cb: () => Awaitable<unknown>) => {
			const started = cancel();

			timeouts[fullid] = setTimeout(() => {
				delete timeouts[fullid];

				/*
				 * The following line is bit of a hack, because we can't be sure that the callback has a Promise.
				 * This lets us use await to force it to be converted to a promise anyway and also allow to forward errors into the correct target.
				 */
				// eslint-disable-next-line no-return-await
				(async() => await cb())().catch((e) => {
					// If there is no error handler, forward to console.error
					if(!error) {
						return console.error(e);
					}

					// another hack to convert an awaitable to a promise.
					// eslint-disable-next-line no-return-await
					(async() => await cb())().catch(console.error);
				});
			}, length);
			return started;
		},
	};
}

/**
 * Create a simple handler to help with providing appropriate feedback to the caller about the status
 * of member timeouts.
 *
 * @param type Special string that helps differentiate different timeouts for a single member.
 * @param target The target member to use the timeout for.
 * @param error The error handler that will receive any errors that may be otherwise missed.
 * @returns A set of helper functions.
 */
export function memberTimeout(type: string, target: GuildMember, error?: (ex: unknown) => Awaitable<unknown>): TimeoutFunctions {
	const id = "U"+ target.guild.id + type + target.id;
	return idTimeout(id, error);
}
