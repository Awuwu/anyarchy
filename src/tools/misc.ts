import {
	ChatInputCommandInteraction, MessagePayload, GuildMember, Message, Snowflake,
	InteractionReplyOptions, InteractionResponse, MappedInteractionTypes, MessageComponentType, RepliableInteraction
} from "discord.js";
import { RatelimitContents, ServerRoleData } from "../index";
import { _HOUR, _MIN } from "./time";
import { getDB } from "./db";

/**
 * Grab a random-ish ID for some action
 *
 * @param base ID of action taker. Usually user ID.
 */
export function getId(base: string): string {
	return (Date.now() % (_HOUR * 1000)) + base + Math.floor(Math.random() * 9999);
}

/**
 * Reply or follow up to an interaction. Convenience method to choose this automatically.
 *
 * @param i The interaction to reply to.
 * @param data The data to send to the interaction.
 */
export function reply(i: RepliableInteraction, data: string|MessagePayload|InteractionReplyOptions): Promise<unknown> {
	return i[ i.replied || i.deferred ? "followUp" : "reply" ](data);
}

/**
 * Reply or follow up to an interaction. Convenience method to choose this automatically.
 *
 * @param i The interaction to reply to.
 * @param data The data to send to the interaction.
 */
export function editMessageOrReply(i: Message|RepliableInteraction, data: string|MessagePayload): Promise<unknown> {
	if(i instanceof Message) {
		return i.edit(data);
	}

	return reply(i, data);
}

/**
 * Checks if the user has a specific role
 *
 * @param target The user to check.
 */
export async function hasRoles(target: GuildMember, roleType: keyof(ServerRoleData)): Promise<boolean> {
	const m = await target.fetch();
	await target.guild.fetch();

	for(const role of config.servers[m.guild.id][roleType]) {
		if(m.roles.cache.has(role)) {
			return true;
		}
	}
	return false;
}

/**
 * Checks if the user has an admin role. If not, sends a message to the user informing that they are not allowed to use the command.
 *
 * @param target The user to check.
 * @param i The interaction to reply to.
 * @returns A boolean indicating whether the user is an admin.
 */
export async function disallowIfNotAdmin(i: ChatInputCommandInteraction, target: GuildMember|null): Promise<boolean> {
	if(target === null) {
		return false;
	}

	// Make sure the build is fully fetched.
	await target.guild.fetch();

	if(await hasRoles(target, "adminRoles")) {
		return true;
	}

	await reply(i, {
		ephemeral: true,
		content: "You are not allowed to use this command.",
	});
	return false;
}

/**
 * Checks if the user has an admin role. If not, sends a message to the user informing that they are not allowed to use the command.
 *
 * @param target The user to check.
 * @param i The interaction to reply to.
 * @returns A boolean indicating whether the user is allowed to use the command.
 */
export async function allowedToUse(i: ChatInputCommandInteraction, target: GuildMember|null, key: keyof(RatelimitContents)): Promise<boolean> {
	if(target === null) {
		return false;
	}

	// Check if the command cooldown has finished
	const r = getDB(target.guild.id, target.id);

	if(Date.now() >= r.ratelimits[key]) {
		// yes; update rate limit and allow command use.
	//	r.ratelimits[key] = Date.now() + (1000 * config.servers[target.guild.id].ratelimit[key]);
		return true;
	}

	// Not allowed to use.
	await reply(i, {
		ephemeral: true,
		content: "You are rate-limited with this command. You can use it in <t:"+ Math.ceil(r.ratelimits[key] / 1000) +":R>",
	});
	return false;
}

/**
 * Undoes command ratelimit when a command fails.
 *
 * @param target The user to manage.
 */
export function resetRatelimit(target: GuildMember|null, key: keyof(RatelimitContents)): void {
	if(target === null) {
		return;
	}

	// Reset the cooldown
	const r = getDB(target.guild.id, target.id);
	r.ratelimits[key] = 0;
}

/**
 * Check the timed component interactions, and cancel whenever it times out.
 *
 * @param i The interaction that originated the components.
 * @param response The expected response from the user.
 * @param baseid The base ID of the interation
 * @returns `undefined` when interaction times out, otherwise the result.
 */
export async function checkTimedInteraction<T extends MessageComponentType>(
	i: ChatInputCommandInteraction, response: InteractionResponse, baseid: string
): Promise<undefined|MappedInteractionTypes<boolean>[T]> {
	try {
		return response.awaitMessageComponent({
			filter: (x) => x.customId.startsWith(baseid), time: _MIN * 1000,
		});

	} catch (e) {
		await i.editReply({ content: "Confirmation timed out.", components: [], });
		return undefined;
	}
}

/**
 * Function to remove all the admin roles from an user.
 *
 * @param target The user to remove roles from
 * @returns The roles removed
 */
export async function removeAdminRoles(target: GuildMember, reason?: string): Promise<Snowflake[]> {
	const roles: Snowflake[] = [];

	for(const role of config.servers[target.guild.id].adminRoles) {
		try {
			// Check if the user had this role
			if(!target.roles.cache.has(role)) {
				continue;
			}

			// Remove the role
			await target.roles.remove(role, reason);
			roles.push(role);

		} catch(_) { /**/ }
	}

	return roles;
}
