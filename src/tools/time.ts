
export const _TOMS = 1000;
export const _SEC = 1;
export const _MIN = _SEC*60;
export const _HOUR = _MIN*60;
export const _DAY = _HOUR*24;
export const _WEEK = _DAY*7;
export const _MON = _DAY*30;
export const _YEAR = _DAY*365;

/**
 * Regexes containing information about time strings.
 */
const timeRegexes = [
	[ _SEC, 	/([\d.]+|a|an)\s*(sec.*|.{1,3}nd.{1}|s)/i, ],
	[ _MIN,		/([\d.]+|a|an)\s*(min.*|.{1,3}te.{1}|m)/i, ],
	[ _HOUR,	/([\d.]+|a|an)\s*(hr.{1}|ho.{1,3})/i, ],
	[ _DAY,		/([\d.]+|a|an)\s*(day.{1}|d)/i, ],
	[ _WEEK,	/([\d.]+|a|an)\s*(we.{1,3}|wk.{1,2}|w)/i, ],
	[ _MON,		/([\d.]+|a|an)\s*(mo.{1,3}|m)/i, ],
	[ _YEAR,	/([\d.]+|a|an)\s*(y.{1,4})/i, ],
] as [ number, RegExp, ][];

/**
 * Function for parsing number of seconds from the supplied time string.
 *
 * @returns negative if failed to parse, otherwise time in seconds.
 */
export function parseTime(time: string): number {
	let str = time;
	let result = 0;

	// Check each regex to see what was put in.
	for(const [ multiplier, regex, ] of timeRegexes) {
		const res = str.match(regex);

		if(!res || res.length <= 0) {
			continue;
		}

		// Delete the match from the input string
		const pos = str.indexOf(res[0]);

		if(pos < 0) {
			// should not happen
			return -1;
		}

		str = str.substring(0, pos) + str.substring(pos + res[0].length);

		// Check for a and an
		const numpart = res[1];
		let value = 1;

		if(!numpart.toLowerCase().startsWith("a") || numpart.length > 2) {
			// Convert it as a float
			value = parseFloat(numpart);

			if(isNaN(value)) {
				return -2;
			}
		}

		// Now we just add it all together
		result += value * multiplier;
	}

	/*
	 * Finally check if there were some parts we didn't cut out, and if so, still and error.
	 * We ignore "and" and some punctuation tho
	 */
	if(str.trim().replace(/(and|,|\.)/i, "").length !== 0) {
		return -1;
	}

	return result;
}

/**
 * Function that converts seconds to a human-readable time string
 */
export function toTimeString(seconds: number): string {
	const parts: string[] = [];
	let sc = seconds;

	/**
	 * Performs a check to see if time string should be included.
	 *
	 * @param str The base string
	 * @param divider The divider to use for determining the number of units
	 * @param min The minimum value for this part to be included.
	 */
	const part = (str: string, divider: number, min: number) => {
		if(sc < min) {
			return;
		}

		// Grab the actual value of units
		const v = Math.floor(sc / divider);
		parts.push(v +" "+ str + (v !== 1 ? "s" : ""));

		// Adjust the current time to remove this unit.
		sc -= v * divider;
	}

	part("year", _YEAR, _MON*15);		// Year check only after 15 months.
	part("month", _MON, _WEEK*8);		// Month check only after 8 weeks.
	part("week", _WEEK, _DAY*10);		// week check only after 10 days.
	part("day", _DAY, _HOUR*30);		// day check only after 30 hours.
	part("hour", _HOUR, _MIN*90);		// hour check only after 90 minutes.
	part("minute", _MIN, _SEC*90);		// minute check only after 90 seconds.
	part("second", _SEC, _SEC);			// second check only after 1 second.

	// Special case where time of 0 seconds will have no content.
	return parts.length === 0 ? "0 seconds" : parts.join(",");
}
