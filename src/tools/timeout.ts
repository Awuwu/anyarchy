import { Awaitable, GuildMember } from "discord.js";
import { removeAdminRoles } from "./misc";
import { getDB } from "./db";
import { fetchMember } from "./fetch";
import { memberTimeout } from "./timers";

function timeoutManager(target: GuildMember) {
	return memberTimeout("T", target);
}

/**
 * Time out an user and handle their roles.
 *
 * @param target The user to time out
 * @param length The amount of time to timeout for.
 * @returns true if succeeded.
 */
export async function timeout(target: GuildMember, length: number, reason?: string): Promise<boolean> {
	try {
		// Remove the admin roles and tell the bot to give the roles back after
		const removed = await removeAdminRoles(target, reason);
		const data = getDB(target.guild.id, target.id);

		// reset all the roles if there is no real timeout.
		if(!data.timeout) {
			data.timeoutRemovedRoles = [];
		}

		// Add the new roles in.
		for(const r of removed) {
			if(!data.timeoutRemovedRoles.includes(r)) {
				data.timeoutRemovedRoles.push(r);
			}
		}

		// Time out the user
		await target.timeout(length, reason);

		// set an event for when the timeout ends.
		await manageTimeout(target, target.communicationDisabledUntil?.valueOf() ?? null);
		return true;

	} catch(ex) {
		console.error(ex);

		// restore roles immediately
		await manageTimeout(target, null);
		return false;
	}
}

/**
 * Set a new timeout to restore user roles.
 *
 * @param ends The end date.
 */
export function manageTimeout(target: GuildMember, ends: number|null): Awaitable<void> {
	const data = getDB(target.guild.id, target.id);
	const man = timeoutManager(target);

	/**
	 * Helper to end the timeout.
	 */
	const handleEnd = async() => {
		for(const role of data.timeoutRemovedRoles) {
			try {
				// Give the user the role back
				await target.roles.add(role, "Role restored after a timeout.");

			} catch(_) { /**/ }
		}

		data.timeoutRemovedRoles = [];
		data.timeout = false;
	}

	// If the timeout is over, then make sure it is cleaned up.
	if(!ends || ends <= Date.now()) {
		man.cancel();
		return handleEnd();
	}

	// Do not track timeout data if there are no roles to add back.
	if(data.timeoutRemovedRoles.length === 0) {
		return;
	}

	// Start the new timeout handler
	data.timeout = true;
	man.start(ends - Date.now(), handleEnd);
}

/**
 * Initialize the timeout handling after the bot restarts
 */
export function initTimeouts(): Promise<unknown> {
	return Promise.all(Object.entries(db).flatMap(([ guildId, d, ]) => {
		return Object.entries(d.users).map(async([ memberId, x, ]) => {
			// ignore if no current timeout.
			if(!x.timeout) {
				return;
			}

			// In case we fail, we should just remove the timeout anyway.
			x.timeout = true;

			// convert to a full member
			const guild = await bot.guilds.fetch(guildId);
			const member = await fetchMember(guild, memberId);

			if(!member) {
				// member is long gone.
				x.timeoutRemovedRoles = [];
				return;
			}

			// update the current timeouts
			await manageTimeout(member, member.communicationDisabledUntil?.valueOf() ?? null);
		})
	}));
}
