import {
	InteractionType, StringSelectMenuBuilder, StringSelectMenuInteraction, StringSelectMenuOptionBuilder,
	ActionRowBuilder, EmbedBuilder, RepliableInteraction, Awaitable, ChatInputCommandInteraction
} from "discord.js";
import { pollTable } from "../commands/poll_commands";
import { hasRoles, reply } from "./misc";
import { DatabasePoll } from "./db";
import { fetchChannelFromURL, fetchGuildFromURL, fetchMember, fetchMessage, fetchTextChannel, parseMessageURL } from "./fetch";
import { idTimeout } from "./timers";

/**
 * Load a manager for polls
 */
function timeoutManager(poll: DatabasePoll) {
	return idTimeout("P"+ poll.url);
}

/**
 * Cancel an active poll. This will remove the poll if it is active and remvoe its entry from the db.
 */
function cancelPoll(poll: DatabasePoll): void {
	// cancel the timeout
	timeoutManager(poll).cancel();

	// Find the poll data index.
	const pd = db[parseMessageURL(poll.url).guild].polls;
	const idx = pd.indexOf(poll);

	if(idx < 0){
		return;
	}

	// now just remove it.
	pd.splice(idx, 1);
}

const MIN_VOTES = 1;

/**
 * End an active poll
 *
 * @param poll The poll data itself
 */
async function endPoll(poll: DatabasePoll): Promise<void> {
	cancelPoll(poll);

	// Get the original guild and target members
	const guild = await fetchGuildFromURL(poll.url);
	const targetMember = await fetchMember(guild, poll.target);

	// Generate the base embed.
	const embed = pollTable[poll.type].finishEmbed(new EmbedBuilder(), poll, targetMember);

	/**
	 * Close the poll with a message
	 *
	 * @param winner The option that won.
	 */
	const closePoll = async(winner: string) => {
		// Update the embed with proper details
		embed.setColor(pollTable[poll.type].embedOptionColors[winner]);

		// Get the original message
		const channel = await fetchChannelFromURL(poll.url);

		if(!channel || !channel.isTextBased()) {
			// wait... something is wrong... The channel could have been deleted.
			return;
		}

		// do the actual option content
		await pollTable[poll.type].finish(poll, winner, targetMember, embed);

		try {
			// Post the new content
			await channel.send({ embeds: [ embed, ], });
			// Delete the original message

			await (await fetchMessage(poll.url))?.delete();
		} catch(_) { /**/ }
	}

	// Check that it had enough votes
	const totalVotes = Object.values(poll.votes).reduce((prev, v) => prev + v.length, 0);

	if(totalVotes < MIN_VOTES) {
		embed.addFields({
			name: "Additional information",
			value: "Too few votes. Needs at least "+ MIN_VOTES +" to finish the poll.",
		});
		return closePoll("z");
	}

	// Has enough votes, check if there is a winner
	let winners: string[] = [];
	let maxVotes = 0;

	for(const [ key, votes, ] of Object.entries(poll.votes)) {
		if(votes.length < maxVotes) {
			continue;
		}

		if(votes.length === maxVotes) {
			winners.push(key);
			continue;
		}

		// more votes, change the vote level
		winners = [ key, ];
		maxVotes = votes.length;
	}

	if(winners.length === 1) {
		// Only a single winner, let the users know.
		for(const [ key, votes, ] of Object.entries(poll.votes)) {
			embed.addFields({
				name: pollTable[poll.type].options[key].label,
				value:
					(winners[0] === key ? "Winner - " : "") +
					votes.length +" vote(s) - "+
					Math.round(votes.length / totalVotes * 100) +"% of total.",
			});
		}

		return closePoll(winners[0]);
	}

	// TODO: create a new poll with only winning options.
	embed.addFields({
		name: "Additional information",
		value: "TODO: implement polling again. For now we just failed.",
	});
	return closePoll("z");
}

export type PollInMessage = EmbedBuilder|RepliableInteraction|null;

/**
 * Function to calculate the percentage of votes for a specific option
 *
 * @param poll The poll data to calculate from
 * @param option The option top check
 * @returns A percentage of the total votes.
 */
export function pollPercentage(poll: DatabasePoll, option: string): string {
	const totalVotes = Object.values(poll.votes).reduce((acc, v) => acc + v.length, 0) || 1;
	return Math.round(poll.votes[option].length / totalVotes * 100) +"%";
}

/**
 * Inform the user the poll failed.
 *
 * @param message The message to edit.
 * @param poll The poll that was used.
 * @param won The option that actually won.
 */
export function pollFailed(message: PollInMessage, poll: DatabasePoll, won: string): Awaitable<unknown> {
	if(!message) {
		return;
	}

	const content = "Poll finished with "+ pollPercentage(poll, won) +" voting against. No action was taken.";

	if(message instanceof EmbedBuilder) {
		// embed builder special
		message.setDescription(content);
		return;
	}

	// interaction.
	return reply(message, content);
}

/**
 * Post a new poll for users to vote on.
 *
 * @param poll The poll contents.
 * @param guild The guild to send the poll in.
 * @param excludedOptions If any options should be excluded from the poll. Allows for polls to be redone.
 */
export async function makePoll(poll: DatabasePoll, i: ChatInputCommandInteraction, excludedOptions: string[]): Promise<boolean> {
	if(!i.guild) {
		await reply(i, {
			ephemeral: true,
			content: "Bot error: Invalid guild.",
		});
		return false;
	}

	// Fetch the allowed options for this poll.
	const options = Object.entries(pollTable[poll.type].options).filter(([ key, ]) => !excludedOptions.includes(key));
	options.unshift([ "_", { label: "Abstain", description: "Your vote will not count.", }, ]);

	if(options.length < 2) {
		await reply(i, {
			ephemeral: true,
			content: "Can not start poll with "+ options.length +" options! At least 2 are needed.",
		});
		return false;
	}

	// Get poll channel. Check if we can post in it.
	const channel = await fetchTextChannel(i.guild, config.servers[i.guild.id].pollChannel);

	if(!channel) {
		await reply(i, {
			ephemeral: true,
			content:
				"Can not start poll because the poll channel "+
				"(<@#"+ config.servers[i.guild.id].pollChannel +">) was not found or it is not a text channel!",
		});
		return false;
	}

	// Fetch some common things.
	const targetMember = await i.guild.members.fetch(poll.target);

	// Generate a menu with options.
	const optionsComponents = new StringSelectMenuBuilder().setCustomId(poll.id +"M");
	optionsComponents.addOptions(... options.map(
		([ key, o, ], i) => {
			// Must ignore abstain votes.
			if(key !== "_") {
				poll.votes[key] = [];
			}

			// Generate the option setting its parts.
			const m = new StringSelectMenuOptionBuilder()
				.setValue(key)
				.setLabel(o.label)
				.setDefault(i === 0);

			if(o.description) {
				// Add the description. We have a few variables we can use dynamically.
				m.setDescription(
					o.description
						.replace("@target", targetMember.displayName)
				);
			}

			return m;
		}
	));

	// Send the message and copy the url!
	const msg = await channel.send({
		content: "@",
		embeds: [
			pollTable[poll.type].startEmbed(new EmbedBuilder(), poll, targetMember),
		],
		components: [
			new ActionRowBuilder<StringSelectMenuBuilder>().addComponents(optionsComponents),
		],
	});

	poll.url = msg.url;

	// Finally, save this into the db!!!
	db[i.guild.id].polls.push(poll);

	// Oh yes also add it to the manager
	const man = timeoutManager(poll);
	man.start(poll.ends - Date.now(), () => endPoll(poll));

	// Inform the user.
	await reply(i, {
		ephemeral: true,
		content: "Started the poll. [Click here to view it.]("+ msg.url +")",
	});
	return true;
}

/**
 * Helper function to initialize the poll event following system.
 */
export function initPolls(): void {
	bot.on("interactionCreate", async(i) => {
		if(i.type === InteractionType.MessageComponent && i.isStringSelectMenu()) {
			await pollEvent(i as StringSelectMenuInteraction);
		}
	});

	// We should now initialize the timeouts for all the polls
	Object.values(db).flatMap((d) => {
		return d.polls.map((poll) => {
			// Create the manager for this specific poll and then exit.
			const man = timeoutManager(poll);
			man.start(poll.ends - Date.now(), () => endPoll(poll));
		});
	});
}

/**
 * Handle a possible poll interaction event.
 *
 * @param i The interaction to handle
 */
function pollEvent(i: StringSelectMenuInteraction) {
	// Find the relevant poll
	if(!i.guildId || !db[i.guildId]) {
		return;
	}

	/**
	 * Set the users votee for a specific key
	 *
	 * @param poll The poll to affect
	 * @param key The key to use
	 */
	const setVote = (poll: DatabasePoll, key: string) => {
		for(const k of Object.keys(poll.votes)) {
			if(k !== key) {
				// remove the user from the current key
				poll.votes[k] = poll.votes[k].filter((x) => x !== i.user.id);
				continue;
			}

			// If this is the one we should vote, add the user to the list if not there already.
			if(!poll.votes[k].includes(i.user.id)) {
				poll.votes[k].push(i.user.id);
			}
		}
	}

	/**
	 * Helper inner function to make coding below simpler. Processes the actual poll.
	 * As long as poll is found in the DB, it should still be ongoing.
	 *
	 * @param poll The actual poll to process
	 */
	const processPoll = async(poll: DatabasePoll) => {
		// Special check for the target of the poll
		if(i.user.id === poll.target) {
			return reply(i, {
				ephemeral: true,
				content: "You can not vote for this poll because you're the target of the poll. Your vote was ignored.",
			});
		}

		// If we can, get the user as a member.
		const member = !i.guild ? null : await fetchMember(i.guild, i.user.id);

		// Special check for roles that can't vote
		if(!member || await hasRoles(member, "cantVote")) {
			return reply(i, {
				ephemeral: true,
				content: "You can not vote for this poll because you have roles that disallow voting. Your vote was ignored.",
			});
		}

		// get the vote ID and check if it is valid.
		const vote = i.values[0] ?? "<null>";

		if(poll.votes[vote]) {
			setVote(poll, vote);
			return reply(i, {
				ephemeral: true,
				content: "Your vote was cast anonymously!",
			});
		}

		// Not valid, just abstain.
		setVote(poll, "_");

		// Check for abstain!
		if(vote === "_") {
			return reply(i, {
				ephemeral: true,
				content: "You have now abstained from voting for this poll.",
			});
		}

		console.error("Invalid vote "+ vote +" in poll "+ poll.id +" of type "+ poll.type);
		return reply(i, {
			ephemeral: true,
			content: "Could not find the option you were looking for. This is a bug in the bot. Your vote was changed to abstain.",
		});
	}

	// Find the real poll and then process it.
	for(const poll of db[i.guildId].polls) {
		if(i.customId.startsWith(poll.id)) {
			return processPoll(poll);
		}
	}
}
