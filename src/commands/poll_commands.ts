import {
	ActionRowBuilder, ButtonBuilder, ButtonStyle, ChatInputCommandInteraction, MessageActionRowComponentBuilder,
	Awaitable, GuildMember, EmbedBuilder
} from "discord.js";
import { allowedToUse, checkTimedInteraction, disallowIfNotAdmin, getId, reply, resetRatelimit } from "../tools/misc";
import { timeout } from "../tools/timeout";
import { DatabasePoll, DatabaseTimeoutPoll } from "../tools/db";
import { PollInMessage, makePoll, pollFailed, pollPercentage } from "../tools/polls";
import { _DAY, _SEC, _TOMS, parseTime, toTimeString } from "../tools/time";
import { toFullMember, toMember } from "../tools/fetch";

/**
 * Table for data about how to process poll info.
 */
type PollTableType = { [key: string]: {
	// The options that can be voted for with this poll.
	options: { [key: string]: {
		label: string,
		description?: string,
	} };
	/**
	 * Build the starting embed based on the poll.
	 */
	startEmbed: (base: EmbedBuilder, poll: DatabasePoll, target: GuildMember) => EmbedBuilder,
	/**
	 * Build the ending embed based on the poll.
	 */
	finishEmbed: (base: EmbedBuilder, poll: DatabasePoll, target: GuildMember|null) => EmbedBuilder,
	/**
	 * The colors the set for the embed based on what option won.
	 */
	embedOptionColors: { [key: string]: number, },
	/**
	 * Function to run when the poll successfully finishes
	 *
	 * @param data The poll data that was submitted.
	 * @param option The option that won out
	 * @param message The message based on the message URL. Can be null if no URL is supplied.
	 */
	finish: (data: DatabasePoll, option: string, target: GuildMember|null, message: PollInMessage) => Awaitable<void>;
} };

export const pollTable: PollTableType = {
	timeout: {
		options: {
			y: { label: "Yes", description: "Vote to time out @target.", },
			z: { label: "No", description: "Vote to not time out.", },
		},
		startEmbed: (base, poll, target) => {
			base
				.setColor(0xC0C040)
				.setFooter({ text: "Vote for a timeout.", })
				.setTimestamp(poll.ends)
				.setTitle("Voting to time out "+ target.displayName)
				.setDescription(
					"A "+ toTimeString((poll as DatabaseTimeoutPoll).length / _TOMS) +
					" timeout to "+ target.toString() +" was requested. Voting ends in "+
					"<t:"+ Math.round(poll.ends / _TOMS) +":R>. Use the menu below to cast your vote."
				);
			return base;
		},
		embedOptionColors: {
			y: 0x40FF40,
			z: 0xFF4040,
		},
		finishEmbed: (base, poll, target) => {
			base
				.setFooter({ text: "A timeout vote ended.", })
				.setTitle("Vote to time out "+ (target?.displayName ?? "a user that left the guild") + " has ended.");
			return base;
		},
		finish: async(poll, won, target, message) => {
			if(won !== "y") {
				// Inform that the poll didn't succeed.
				await pollFailed(message, poll, won);
				return;
			}

			// Do the actual timing out
			let extra = "The user was timed out.";

			if(!target || !await timeout(target, (poll as DatabaseTimeoutPoll).length, "Timeout command")) {
				if(message && !(message instanceof EmbedBuilder)) {
					await reply(message, {
						ephemeral: true,
						content: "I was unable to time out <@"+ poll.target +">.",
					});
					return;
				}

				extra = "Was unable to time out the user.";
			}

			// Inform about success.
			if(!message) {
				return;
			}

			if(message instanceof EmbedBuilder) {
				// If message is an EmbedBuilder, this was a full poll
				message.setDescription(
					"Poll finished with "+ pollPercentage(poll, won) +" voting for timing out <@"+ poll.target +">. "+ extra
				);

			} else {
				// Instead it is an interation. Send a message to the channel.
				await reply(message, "<@"+ poll.target +"> was timed out.");
			}
		},
	},
};

/**
 * The `/timeout` command handler
 */
export async function E_TIMEOUT(i: ChatInputCommandInteraction): Promise<void> {
	const submitter = await toFullMember(i.guild, i.member);

	if(!await disallowIfNotAdmin(i, submitter)) {
		return;
	}

	// Attempt to parse a time string
	const time = parseTime(i.options.getString("duration", true));

	if(time < 0) {
		// could not understand
		await i.reply({
			ephemeral: true,
			content: "I was not able to understand the duration you asked for. Please try again.",
		});
		return;
	}

	const target = i.options.getUser("user", true);
	const pollText = time > 60 ? " If yes, a poll will be set up to vote whether to apply the mute." : "";

	// Check time restrictions
	if(time > 60 && !await allowedToUse(i, submitter, "timeout")) {
		return;
	}

	if(time > _DAY) {
		resetRatelimit(submitter, "timeout");
		await i.reply({
			ephemeral: true,
			content: "`"+ toTimeString(time) +"`? Well that's awfully long isn't it. The most I can do is 1 day!",
		});
		return;
	}

	if(time < 30 * _SEC) {
		resetRatelimit(submitter, "timeout");
		await i.reply({
			ephemeral: true,
			content: "`"+ toTimeString(time) +"`? Well that's awfully short isn't it. The least I can do is 30 seconds!",
		});
		return;
	}

	// Ask the user if we understood.
	const baseid = getId(i.user.id);

	const cb = new ButtonBuilder().setCustomId(baseid +"y").setLabel("Yes").setStyle(ButtonStyle.Success);
	const db = new ButtonBuilder().setCustomId(baseid +"n").setLabel("No").setStyle(ButtonStyle.Secondary);
	const row = new ActionRowBuilder<MessageActionRowComponentBuilder>().addComponents(cb, db);

	const response = await i.reply({
		ephemeral: true,
		content: "You asked to time out "+ target.toString() +" for `"+ toTimeString(time) +"`. Is this correct?"+ pollText,
		components: [ row, ],
	});

	// Wait for the user to confirm. Bail if they don't
	const confirm = await checkTimedInteraction(i, response, baseid);

	if(!confirm) {
		resetRatelimit(submitter, "timeout");
		return;
	}

	// Check if the user confirmed the question.
	if(confirm.customId.endsWith("n")) {
		resetRatelimit(submitter, "timeout");
		await i.editReply({
			components: [],
			content: "Use the command again if you wish to try again.",
		});
		return;
	}

	// yeet components.
	await i.editReply({ components: [], });

	const poll: DatabaseTimeoutPoll = {
		type: "timeout",
		id: baseid,
		ends: Date.now() + (time * _TOMS * config.servers[i.guild?.id ?? ""]?.pollLengths.timeout),
		length: time * _TOMS,
		// Target of the poll. This is typically a user ID.
		target: target.id,
		url: "",		// This will be filled in when the poll is posted.
		votes: {},
	};

	if(time <= 60) {
		// Immediately resolve for any timeout up to 60 seconds.
		await pollTable[poll.type].finish(poll, "y", await toMember(i.guild, target), confirm);
		return;
	}

	// Make the poll and if it fails, reset the rate limit.
	if(!await makePoll(poll, i, [])) {
		resetRatelimit(submitter, "timeout");
	}
}

/**
 * The `/rehab` command handler
 */
export async function E_REHAB(i: ChatInputCommandInteraction): Promise<void> {
	await i.reply({
		ephemeral: false,
		content: "todo",
	});
}

/**
 * The `/punish` command handler
 */
export async function E_PUNISH(i: ChatInputCommandInteraction): Promise<void> {
	await i.reply({
		ephemeral: false,
		content: "todo",
	});
}

/**
 * The `/unpunish` command handler
 */
export async function E_UNPUNISH(i: ChatInputCommandInteraction): Promise<void> {
	await i.reply({
		ephemeral: false,
		content: "todo",
	});
}
