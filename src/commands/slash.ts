import {
	ApplicationCommandOptionAllowedChannelTypes, Awaitable, ChatInputCommandInteraction, InteractionType, REST, Routes,
	SlashCommandBuilder, SlashCommandSubcommandBuilder, SlashCommandSubcommandGroupBuilder
} from "discord.js";
import { e } from "../index";
import * as R from "./role";
import * as P from "./poll_commands";

/**
 * Type of the command option. This is used to determine how to build option data.
 */
type CommandOptionType = "user" | "channel" | "role" | "mention" | "bool" | "int" | "number" | "string" | "attach";

/**
 * Extra data only allowed in top level slash context.
 */
type CommandTopData = {
	nsfw: boolean,
};

/**
 * The data for each option. The specific fields depend on the `CommandOptionType`
 */
type CommandOption = {
	type: CommandOptionType,
	description: string,
	required: boolean,

	channelTypes?: ApplicationCommandOptionAllowedChannelTypes[],
	choices?: { [key:string]: number, }|{ [key:string]: string, },

	max?: number,
	min?: number,
};

/**
 * A list of sub-commands.
 */
type CommandGroup = {
	description: string,
	sub: { [key:string]: CommandInfo, },
}

/**
 * The data for top level group.
 */
type CommandGroupTop = CommandGroup & CommandTopData;

/**
 * The data for each command.
 */
type CommandInfo = {
	description: string,
	options: { [key:string]: CommandOption, },
	execute: (ínteraction: ChatInputCommandInteraction) => Awaitable<void>,
};

/**
 * The data for top level command.
 */
type CommandInfoTop = CommandInfo & CommandTopData;

/**
 * ===========================================
 * COMMANDS
 * ===========================================
 */
const ROLE: CommandGroupTop = {
	description: "Commands relating to roles in the server.",
	nsfw: false,
	sub: {
		give: {
			description: "Give another user a role. Only special roles can be given.",
			options: {
				role: {
					type: "role",
					description: "The target role.",
					required: true,
				},
				user: {
					type: "user",
					description: "The target user.",
					required: true,
				},
			},
			execute: R.E_ROLE_GIVE,
		},
		take: {
			description: "Take a role from yourself or another user. Only special roles can be taken.",
			options: {
				role: {
					type: "role",
					description: "The target role.",
					required: true,
				},
				user: {
					type: "user",
					description: "The target user. If not given, will default to you.",
					required: false,
				},
			},
			execute: R.E_ROLE_TAKE,
		},
	},
};

const PING: CommandInfoTop = {
	description: "test",
	options: {},
	nsfw: false,
	execute: async(i: ChatInputCommandInteraction) => {
		await i.reply({
			ephemeral: false,
			content: "Pong!",
		});
	},
};

const TIMEOUT: CommandInfoTop = {
	description: "Time out a user. If than 1 minute, a poll is put up. If a user is muted, timeouts can not be given.",
	options: {
		user: {
			type: "user",
			description: "The target user. Can not be you.",
			required: true,
		},
		duration: {
			type: "string",
			description: "Text for the timeout length. For example, \"1 minute\".",
			required: true,
		},
	},
	nsfw: false,
	execute: P.E_TIMEOUT,
};

const REHAB: CommandInfoTop = {
	description: "Send user to rehab. A poil is put up to decide if the community agrees.",
	options: {
		user: {
			type: "user",
			description: "The target user. Can not be you.",
			required: true,
		},
	},
	nsfw: false,
	execute: P.E_REHAB,
};

const PUNISH: CommandInfoTop = {
	description: "Put up a poll asking if a user should be punished.",
	options: {
		user: {
			type: "user",
			description: "The target user. Can not be you.",
			required: true,
		},
	},
	nsfw: false,
	execute: P.E_PUNISH,
};

const UNPUNISH: CommandInfoTop = {
	description: "Put up a poll asking if a user should have their punishment removed.",
	options: {
		user: {
			type: "user",
			description: "The target user. Can not be you.",
			required: true,
		},
	},
	nsfw: false,
	execute: P.E_UNPUNISH,
};

/**
 * COMMANDS TREE
 */
const COMMANDS: { [key:string]: (CommandInfo | CommandGroup) & { nsfw: boolean, }, } = {
	ping: PING,
	role: ROLE,
	rehab: REHAB,
	punish: PUNISH,
	unpunish: UNPUNISH,
	timeout: TIMEOUT,
};

/**
 * Add an option to the `SlashCommandBuilder` instance.
 *
 * @param key The name of the option
 * @param value The data of the option
 * @param s The `SlashCommandBuilder` instance to use
 */
function sendOption(key: string, value: CommandOption, s: SlashCommandSubcommandBuilder | SlashCommandBuilder) {
	switch(value.type) {
		case "attach":
			s.addAttachmentOption((opt) => opt
				.setName(key)
				.setDescription(value.description)
				.setRequired(value.required)
			);
			return;

		case "mention":
			s.addMentionableOption((opt) => opt
				.setName(key)
				.setDescription(value.description)
				.setRequired(value.required)
			);
			return;

		case "user":
			s.addUserOption((opt) => opt
				.setName(key)
				.setDescription(value.description)
				.setRequired(value.required)
			);
			return;

		case "channel":
			s.addChannelOption((opt) => { opt
				.setName(key)
				.setDescription(value.description)
				.setRequired(value.required)

				if(value.channelTypes) {
					opt.addChannelTypes(...value.channelTypes);
				}

				return opt;
			});
			return;

		case "role":
			s.addRoleOption((opt) => opt
				.setName(key)
				.setDescription(value.description)
				.setRequired(value.required)
			);
			return;

		case "bool":
			s.addBooleanOption((opt) => opt
				.setName(key)
				.setDescription(value.description)
				.setRequired(value.required)
			);
			return;

		case "int":
			s.addIntegerOption((opt) => { opt
				.setName(key)
				.setDescription(value.description)
				.setRequired(value.required)

				if(value.max) {
					opt.setMaxValue(value.max);
				}

				if(value.min) {
					opt.setMinValue(value.min);
				}

				if(value.choices) {
					opt.setChoices(... Object.entries(value.choices).map(
						([ name, v, ]) => { return { name, value: v as number, }; }
					));
				}

				return opt;
			});
			return;

		case "number":
			s.addNumberOption((opt) => { opt
				.setName(key)
				.setDescription(value.description)
				.setRequired(value.required)

				if(value.max) {
					opt.setMaxValue(value.max);
				}

				if(value.min) {
					opt.setMinValue(value.min);
				}

				if(value.choices) {
					opt.setChoices(... Object.entries(value.choices).map(
						([ name, v, ]) => { return { name, value: v as number, }; }
					));
				}

				return opt;
			});
			return;

		case "string":
			s.addStringOption((opt) => { opt
				.setName(key)
				.setDescription(value.description)
				.setRequired(value.required)

				if(value.max) {
					opt.setMaxLength(value.max);
				}

				if(value.min) {
					opt.setMinLength(value.min);
				}

				if(value.choices) {
					opt.setChoices(... Object.entries(value.choices).map(
						([ name, v, ]) => { return { name, value: v as string, }; }
					));
				}

				return opt;
			});
			return;
	}
}

/**
 * Recursive function to generate the full tree of slash commands.
 *
 * @param sub The subgroup we're currently processing.
 * @returns The data for the current subgroup and its children.
 */
function processSubCommand(top: boolean, key: string, value: CommandInfo | CommandGroup):
	SlashCommandBuilder | SlashCommandSubcommandGroupBuilder | SlashCommandSubcommandBuilder {
	// Create the slash command context based on the current status... this is kind of ugly. blah.
	const s = (
		top ? new SlashCommandBuilder() :
		"sub" in value ? new SlashCommandSubcommandGroupBuilder() :
		new SlashCommandSubcommandBuilder()
	)
		.setName(key)
		.setDescription(value.description);

	if(top) {
		// Top commands have some special fields
		(s as SlashCommandBuilder)
			.setDMPermission(false)
			.setNSFW((value as CommandInfoTop | CommandGroupTop).nsfw);
	}

	if(!("sub" in value)) {
		// Process a normal sub-command
		for(const [ k, v, ] of Object.entries(value.options)) {
			sendOption(k, v, s as SlashCommandSubcommandBuilder | SlashCommandBuilder);
		}

	} else {
		// process a sub-command group
		for(const [ k, v, ] of Object.entries(value.sub)) {
			const list = "sub" in v;

			// Create either sub-command group or a simple subcommand
			if(list) {
				(s as SlashCommandBuilder).addSubcommandGroup(processSubCommand(false, k, v) as SlashCommandSubcommandGroupBuilder);

			} else {
				(s as SlashCommandBuilder).addSubcommand(processSubCommand(false, k, v) as SlashCommandSubcommandBuilder);
			}
		}
	}

	return s;
}

/**
 * Send the slash command data to Discord API
 */
export async function sendCommands(): Promise<void> {
	const rest = new REST().setToken(config.login.token);

	if(!bot.application) {
		e("bot.application is undefined!");
		return;
	}

	// Convert all commands to slash command format
	const commands: unknown[] = [];

	for(const [ key, value, ] of Object.entries(COMMANDS)) {
		commands.push(processSubCommand(true, key, value));
	}

	await rest.put(Routes.applicationCommands(bot.application.id), { body: commands, });
//	await rest.put(Routes.applicationGuildCommands(bot.application.id, "1125439939094519889"), { body: commands, });
}

/**
 * Create the slash command handler.
 */
export function initCommandHandler(): void {
	// Create the interaction manager
	bot.on("interactionCreate", async(i) => {
		if(i.type === InteractionType.ApplicationCommand) {
			// Fetch the command data. If this is a sub-command, fetch the sub-command info.
			let data = COMMANDS[i.commandName] as CommandInfo | CommandGroup;

			if("sub" in data) {
				data = data.sub[(i as ChatInputCommandInteraction).options.getSubcommand()];
			}

			try {
				// Execute the actual command. More handling to do here if necessary?
				await data.execute(i as ChatInputCommandInteraction);

			} catch(error) {
				console.error(error);

				// Let the user know of failures. Detects how to reply to the user (either followUp or reply).
				await i[ i.replied || i.deferred ? "followUp" : "reply" ]({
					content: "Error while attempting to use the command. Please let an admin know, if this continues happening.",
					ephemeral: true,
				});
			}
		}
	});
}
