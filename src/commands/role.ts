import { ChatInputCommandInteraction } from "discord.js";
import { allowedToUse, disallowIfNotAdmin, resetRatelimit } from "../tools/misc";
import { toFullMember, toMember } from "../tools/fetch";

/**
 * The `/role give` command handler
 */
export async function E_ROLE_GIVE(i: ChatInputCommandInteraction): Promise<void> {
	const submitter = await toFullMember(i.guild, i.member);

	if(!await disallowIfNotAdmin(i, submitter)) {
		return;
	}

	// check if the user is on the server
	const user = i.options.getUser("user", true);
	const member = await toMember(i.guild, user);

	if(!member) {
		await i.reply({
			ephemeral: true,
			content: "Looks like "+ user.toString() +" is not on the server!",
		});
		return;
	}

	// Check if the user has the role
	const role = i.options.getRole("role", true);

	if(member.roles.cache.has(role.id)) {
		await i.reply({
			ephemeral: true,
			content: user.toString() +" already has the "+ role.toString() +" role!",
		});
		return;
	}

	// Check time restrictions
	if(!await allowedToUse(i, submitter, "role")) {
		return;
	}

	try {
		// Give the actual role
		await member.roles.add(role.id);

		await i.reply({
			ephemeral: true,
			content: user.toString() +" was given the "+ role.toString() +" role!",
		});

	} catch(ex) {
		// Failed....
		resetRatelimit(submitter, "role");
		await i.reply({
			ephemeral: true,
			content: "Was not able to give "+ user.toString() +" the "+ role.toString() +" role.",
		});
	}
}

/**
 * The `/role take` command handler
 */
export async function E_ROLE_TAKE(i: ChatInputCommandInteraction): Promise<void> {
	const submitter = await toFullMember(i.guild, i.member);
	const target = i.options.getUser("user", false);

	// Check if the submitter is targeting themselves.
	if((target && target.id !== i.user.id) && !await disallowIfNotAdmin(i, submitter)) {
		return;
	}

	// check if the user is on the server
	const member = await toMember(i.guild, target ?? i.user);

	if(!member) {
		await i.reply({
			ephemeral: true,
			content: "Looks like "+ (target ?? i.user).toString() +" is not on the server!",
		});
		return;
	}

	// Check if the user has the role
	const role = i.options.getRole("role", true);

	if(!member.roles.cache.has(role.id)) {
		await i.reply({
			ephemeral: true,
			content: member.toString() +" doesn't have the "+ role.toString() +" role!",
		});
		return;
	}

	// Check time restrictions
	if(!await allowedToUse(i, submitter, "role")) {
		return;
	}

	try {
		// Take the actual role
		await member.roles.add(role.id);

		await i.reply({
			ephemeral: true,
			content: member.toString() +" had the "+ role.toString() +" role taken!",
		});

	} catch(ex) {
		// Failed....
		resetRatelimit(submitter, "role");
		await i.reply({
			ephemeral: true,
			content: "Was not able to take the "+ role.toString() +" role from "+ member.toString() +".",
		});
	}
}
