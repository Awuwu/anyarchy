/* eslint-disable no-var */
/* eslint-disable vars-on-top */
import { Client, Snowflake } from "discord.js";
import { Config } from "./index";
import { Database } from "./tools/db";

export {};

declare global {
	var config: Config;
	var bot: Client;
	var db: { [key: Snowflake]: Database };
}
