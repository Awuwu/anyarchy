# Anyarchy - an anarchist Discord bot

This bot is designed to assist servers be less hierarchical and support decisions via popular vote. It is configurable and designed to be used in a single or multiple servers. It is written in Typescript and designed to bed used on a Node.js server.

To see the written specifications the bot is based on, [click here to go to the specs](specs.md)

## Bot config

Since the configuration of the bot contains private information, it is not provided. Here is an example configuration:

```json
{
	"autosaveSeconds": 300,
	"login": {
		"token": "<bot token>",
		"id": "<bot id>"
	},
	"servers": {
		"<server id>": {
			"specialRoles": [
				...
			],
			"adminRoles": [
				...
			],
			"pollRoles": [
				...
			],
			"cantVote": [
				...
			],
			"pollChannel": "<channel or thread>",
			"ratelimit": {
				"unpunish": 86400,
				"punish": 86400,
				"timeout": 86400,
				"role": 86400
			},
			"pollLengths": {
				"unpunish": 86400,
				"punish": 86400,
				"rehab": 86400,
				"timeout": 0.25
			}
		}
	}
}
```
